<?php
require_once 'Controlador/logica/carrito.php';
require_once 'Controlador/logica/pedido.php';
include 'Vista/presentacion/menuCliente.php';
require_once 'Controlador/logica/compra.php';
$cli = new cliente($_SESSION["id"]);
$idCli = $cli->getId();
$c = new carrito("", $idCli);
$compra = new compra();
$idc = $compra->getIdCompra();
$car = new carrito("", $idCli);
$cantidades = $car ->  traerCant();//trae cantidades de los productos
$idEmpleado = rand(1,3);
$idP = $car->traerProducto();//array productos
$objPedido = new pedido();
$idrandom = rand(1000000, 9999999);
if (isset($_POST['namebtncompra'])) {
	$com = new compra($idrandom, "", $idCli);
	$com->agregarCompra();
	$objPedido = new pedido("", $idrandom, $idCli, $idEmpleado);
	$objPedido->insertarpedido();
	$objPedido->borrarCarrito();
	echo '<script language="javascript">alert("Compra terminada... Reclame su pedido!!");</script>';
}
$cont = 0;
$total =0;
foreach ($idP as $idPP) {
	$idpp = $idPP->getId();
	$nombre = $idPP->getNombre();
	$precio = $idPP ->getPrecio();
	$subtotal = $precio * $cantidades[$cont];
	$total += $subtotal;
	$historial = new historial("", "", $idpp, $nombre, $idrandom, $idCli, $cantidades[$cont],$precio, $subtotal, $total,$idEmpleado);
	$historial->agregarHistorial();
	$cont ++;
}

$info = $car->verCarrito();
?>
<div class="container">
	<div class="row mt-3">
		<div class="col">
			<div class="card">
				<h5 class="card-header">Consulta del Carrito</h5>
				<div class="card-body">
					<table class="table table-striped table-hover">
						<thead>
							<tr>

								<th scope="col" colspan="4" class="text-center table-warning">Producto</th>

							</tr>
							<tr>
								<th scope="col">Nombre Producto</th>
								<th scope="col">Descripcion</th>
								<th scope="col">precio Unitario</th>
								<th scope="col">cantidad</th>
							</tr>
						</thead>
						<tbody>
                            <?php
                            $i = 0;
                         
                            $idpp = 0;
                            foreach ($info as $var) {
                            	echo "<tr>";
                            	
                            	echo "<td>" . $var->getNombre() . "</td>";
                            	echo "<td>" . $var->getDescripcion() . "</td>";
                            	echo "<td>" . $var->getPrecio() . "</td>";
                            	echo "<td>" . $cantidades[$idpp] . "</td>";
                     
                            	$i += $var->getPrecio() * $cantidades[$idpp];
                    
                            	
                           		$idpp = $idpp +1;
                            	echo "</tr>";
                            	
                            	// echo "<td>" . $i . "</td>";
                            }
                            ?>
							<tr>
								<td colspan="2">TOTAL</td>
								<td> <?php echo $i;?></td>
							</tr>
						</tbody>
					</table>

					<form action="" method="post">
						<button class="btn btn-success" name="namebtncompra"
							value="Agregar" type="submit">TERMINAR COMPRA</button>

					</form>

				</div>

			</div>

		</div>
	</div>
</div>
