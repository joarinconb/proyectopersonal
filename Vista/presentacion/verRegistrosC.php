<?php
require_once 'Controlador/logica/compra.php';
require_once 'Controlador/logica/pedido.php';
require_once 'Controlador/logica/cliente.php';
include 'Vista/presentacion/menuAdministrador.php';
$cliente = new cliente();
$clientes = $cliente->consultarTodo();
?>
<div class="container">
    <div class="row mt-3">
        <div class="col">
            <div class="card">
                <h5 class="card-header">Clientes</h5>
                <div class="card-body">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>

                                <th scope="col" colspan="4" class="text-center table-warning">Pedidos</th>

                            </tr>
                            <tr>
                                <th scope="col">Nombre</th>
                                <th scope="col">Dirección</th>
                                <th scope="col">Telefono</th>
                                <th scope="col">Correo</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
						foreach ($clientes as $clienteactual) {
							echo "<tr>";
							echo "<td>" . $clienteactual->getNombre() . "</td>";
							echo "<td>" . $clienteactual->getDireccion() . "</td>";
							echo "<td>" . $clienteactual->getTelefono() . "</td>";
							echo "<td>" . $clienteactual->getCorreo() . "</td>";
							echo "</tr>";
						}
						?>

                        </tbody>

                    </table>


                </div>
            </div>
        </div>
    </div>
</div>