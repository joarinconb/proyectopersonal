<?php
require_once 'Controlador/logica/Administrador.php';
include 'Vista/presentacion/menuAdministrador.php';

$administrador = new Administrador($_SESSION["id"]);
$administrador -> consultar();
?>
<div class="container">
	<div class="row mt-3">
		<div class="col">
			<div class="card">
				<h5 class="card-header"><?php echo $administrador -> getNombre(); ?></h5>
				<div class="card-body">
				</div>
			</div>
		</div>
	</div>
</div>
