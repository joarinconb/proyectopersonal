<?php
include 'Vista/presentacion/menuCliente.php';
require_once 'Controlador/logica/carrito.php';
require_once 'Controlador/logica/compra.php';
$cli = new cliente($_SESSION["id"]);
$idCli = $cli->getId();
$hostorial = new historial("","","","","",$idCli);
$histo = $hostorial -> verHistorial();
$car = new carrito("", $idCli);

?>
<div class="container">
    <div class="row mt-3">
        <div class="col">
            <div class="card">
                <h5 class="card-header">Consulta de las compras</h5>
                <div class="card-body">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>

                                <th scope="col" colspan="7" class="text-center table-warning">Compras Realizadas</th>

                            </tr>
                            <tr>
                                <th scope="col">Id de la compra</th>
                                <th scope="col">fecha</th>
                                <th scope="col">productos</th>
                                <th scope="col">cantidad</th>
                                <th scope="col">precio</th>
                                <th scope="col">subtotal</th>
                                <th scope="col">Total</th>

                            </tr>
                        </thead>
                        <tbody>
						<?php
							$cont =0;
							$totall = 0;
							$total1 = 0;
							$total2 = 0;
                            foreach($histo as $var) {		
								if($cont == 0){
									$varId = "";
									echo "<tr>";
									echo "<td>" . $var->getIdCompra() . "</td>";
									echo "<td>" . $var->getFecha() . "</td>";
									echo "<td>" . $var-> getNombreProducto() . "</td>";
									echo "<td>" . $var-> getcantidad() . "</td>";
									echo "<td>" . $var-> getprecio() . "</td>";
									echo "<td>" . $var-> getsubtotal() . "</td>";
									$total1 +=  $var-> getsubtotal();
									echo "<td> <b> " . $total1. "</td>";
									$varId = $var->getIdCompra();
								
								
								}else if($varId == $var->getIdCompra() && $cont>0){
									echo "<tr>";
									echo "<td></td>";
									echo "<td></td>";
									echo "<td>" . $var-> getNombreProducto() . "</td>";
									echo "<td>" . $var-> getcantidad() . "</td>";
									echo "<td>" . $var-> getprecio() . "</td>";
									echo "<td>" . $var-> getsubtotal() . "</td>";
									$total2 +=  $var-> getsubtotal();
									$totall = $total1+$total2;
								
									echo "<td> <b>" . $totall. "</td>";
								
							
								}else if($varId != $var->getIdCompra() && $cont >0){
									$total1 =0;
									$total2 =0;
									echo "<tr>";
									echo "<td>" . $var->getIdCompra() . "</td>";
									echo "<td>" . $var->getFecha() . "</td>";
									echo "<td>" . $var-> getNombreProducto() . "</td>";
									echo "<td>" . $var-> getcantidad() . "</td>";
									echo "<td>" . $var-> getprecio() . "</td>";
									echo "<td>" . $var-> getsubtotal() . "</td>";
									
									$total1 +=  $var-> getsubtotal();
								
								
									$totall = $total1+$total2;
									echo "<td> <b>" . $totall. "</td>";
									$varId = $var->getIdCompra();
								
								}
                            	$cont ++;
                            	
                            }
                            ?>
                            
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>