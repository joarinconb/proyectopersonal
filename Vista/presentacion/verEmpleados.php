<?php
include 'Vista/presentacion/menuAdministrador.php';
require_once 'Controlador/logica/empleado.php';
$empleado = new empleado();
$idEmpleado = $empleado -> consultarDatos();
?>
<div class="container">
    <div class="row mt-3">
        <div class="col">
            <div class="card">
                <h5 class="card-header">Consultar todos los Empleados</h5>
                <div class="card-body">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th scope="col" colspan="4" class="text-center table-warning">Empleados</th>
                            </tr>
                            <tr>
                                <th scope="col">Id del Empleado</th>
                                <th scope="col">Nombre </th>
                                <th scope="col">Telefono</th>
                                 <th scope="col">Correo</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                 
                            foreach($idEmpleado as $var) {
                            	
                            		echo "<tr>";
                            		echo "<td>" . $var->getId() . "</td>";
                            		echo "<td>" . $var->getNombre() . "</td>";
                            		echo "<td>" . $var->getTelefono() . "</td>";
                            		echo "<td>" . $var-> getCorreo() . "</td>";
                            	
                            }
							?>

                        </tbody>

                    </table>


                </div>
            </div>
        </div>
    </div>
</div>
