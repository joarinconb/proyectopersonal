<?php 
require_once 'modelo/persistencia/Conexion.php';
require_once 'modelo/persistencia/pedidoDAO.php';

class pedido{
    private $idPedido;
    private $idCompra;
    private $idCliente;
    private $idEmpleado;
    private $pedidoDAO;
   // private $idProducto;

    public function getIdPedido()
	{
		return $this->idPedido;
	}

    public function getIdCompra()
	{
		return $this->idCompra;
	}

    public function getIdCliente()
	{
		return $this->idCliente;
	}

    public function getIdEmpleado()
	{
		return $this->idEmpleado;
	}

    public function getIdProducto()
	{
		return $this->idProducto;
	}



    public function __construct($idPedido = "", $idCompra = "", $idCliente = "", $idEmpleado="")
    {
        
       $this -> idPedido = $idPedido; 
       $this -> idCompra = $idCompra; 
       $this -> idCliente = $idCliente;
       $this -> idEmpleado = $idEmpleado;
      // $this -> idProducto = $idProducto;
       $this -> conexion = new Conexion();
       $this -> pedidoDAO = new pedidoDAO(rand(1000000,9999999),$idCompra,$idCliente,$idEmpleado);
       //$this -> sentencia = $this ->pedidoDAO ->insertarPedido(); 
       //$this ->pedidoDAO ->borrarCarrito();
    }

    public function insertarpedido(){
    $this -> conexion -> abrir();
    $this -> conexion -> ejecutar($this ->pedidoDAO ->insertarPedido()); 
    $this -> conexion -> cerrar();
    }
    
    public function borrarCarrito(){
    	$this -> conexion -> abrir();
    	$this ->conexion -> ejecutar($this -> pedidoDAO ->borrarCarrito());
    	$this -> conexion -> cerrar();
    }
    
    
    public function consultarPedidos(){
    	$this -> conexion -> abrir();
    	$this -> conexion -> ejecutar($this -> pedidoDAO -> consultarPedidos());
    	$pedidos = array();
    	while(($registro = $this -> conexion -> extraer()) != null){
    		$pedido = new pedido($registro[0], $registro[1], $registro[2], $registro[3]);
    		array_push($pedidos, $pedido);
    	}
    	$this -> conexion -> cerrar();
    	return  $pedidos;
    }
    
    public function consultarEmpleado(){
    	$this -> conexion -> abrir();
    	echo $this -> pedidoDAO -> consultarPedidos();
    	$this -> conexion -> ejecutar($this -> pedidoDAO -> consultarPedidos());
    	$pedidos = array();
    	while(($registro = $this -> conexion -> extraer()) != null){
    		$pedido = new pedido($registro[0], $registro[1], $registro[2], $registro[3]);
    		array_push($pedidos, $pedido);
    	}
    	$this -> conexion -> cerrar();
    	return  $pedidos;
    }


    
}



?>