<?php 
require_once 'modelo/persistencia/Conexion.php';
require_once 'modelo/persistencia/compraDAO.php';
class compra{
    private $idCompra;
    private $date;
    private $idClienteCom;
    private $estado;
    private $conexion;
    private $compraDAO;
    private $objPedido;

    

	public function getEstado()
	{
		return $this->estado;
	}

	public function getIdCompra()
	{
		return $this->idCompra;
	}

    public function getDate()
	{
		return $this->date;
	}

    public function getIdClienteCom()
	{
		return $this->idClienteCom;
	}

	/**
	 * @param string $estado
	 */
	public function setEstado($estado)
	{
		$this->estado = $estado;
	}

	public function __construct($idCompra = "", $date = "", $idClienteCom = "", $estado="")
    {
        //echo nl2br("entró a constructo de compra ".$idClienteCom."\n Id compra= ".$idCompra."\n"); 
        $this -> idCompra = $idCompra; 
        $this -> date = $date;
        $this -> idClienteCom = $idClienteCom;
        $this -> estado = $estado;
        $this -> conexion = new Conexion();
        $this -> compraDAO = new compraDAO($idCompra,"", $idClienteCom, $estado);
        //$this -> objPedido = new pedido("",$idCompra,$idClienteCom,"");

    }

    public function agregarCompra(){
        $this ->conexion ->abrir();
        //echo nl2br("\n Agregar compra sirve, id de la compra: ".$this ->idCompra."\n Id del cliente: ".$this ->idClienteCom."");
        $this ->conexion ->ejecutar($this ->compraDAO->insertarCompra());
        //$this -> objPedido ->recibirPedido();
        $this ->conexion ->cerrar();
    }

 
    
    public function traerCompra(){
    	$this ->conexion ->abrir();
    	$this ->conexion ->ejecutar($this ->compraDAO->traerCompra());
    	$this ->conexion ->cerrar();
    }
    
    
    
    public function ConsultarCompras(){
    	$this -> conexion -> abrir();
    	$this -> conexion -> ejecutar($this -> compraDAO -> ConsultarCompras());
    	$compras = array();
    	while(($registro = $this -> conexion -> extraer()) != null){
    		$compra = new compra($registro[0], $registro[1], $registro[2],$registro[3]);
    		array_push($compras, $compra);
    	}
    	$this -> conexion -> cerrar();
    	return  $compras;
    }
    
    public function editarEstado(){
    	$this -> conexion -> abrir();
    	$this -> conexion -> ejecutar($this -> compraDAO -> editarEstado());
    	$this -> conexion -> cerrar();
    }
    
}





?>